import { BASE_URL, createConfig } from "./configURL";
import axios from "axios";

export const userService = {
  postDangNhap: (dataUser) => {
    return axios({
      data: dataUser,
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      headers: createConfig(),
    });
  },
};
