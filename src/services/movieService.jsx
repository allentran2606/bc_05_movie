import { BASE_URL, createConfig } from "./configURL";
import axios from "axios";

export const movieService = {
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`,
      method: "GET",
      headers: createConfig(),
    });
  },

  getHeThongRap: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
