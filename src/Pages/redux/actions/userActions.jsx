import { SET_USER_INFOR } from "../constant/userConstant";
import { userService } from "../../../services/userService";
import { userLocalStorage } from "../../../services/localStorageService";
import { message } from "antd";

export const setLoginAction = (value) => {
  // Call api here

  return {
    type: SET_USER_INFOR,
    payload: value,
  };
};

export const setLoginActionService = (userForm, onSuccess) => {
  return (dispatch) => {
    userService
      .postDangNhap(userForm)
      .then((res) => {
        message.success("Login success!");

        // Lưu vào localStore
        userLocalStorage.set(res.data.content);

        console.log(res);

        // Đẩy lên redux store
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("You dumbass!");
        console.log(err);
      });
  };
};
