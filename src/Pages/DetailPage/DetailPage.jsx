import React from "react";
import { useParams } from "react-router-dom";
import Header from "../../Components/Header/Header";

export default function DetailPage() {
  let params = useParams();
  return <div>{params.id}</div>;
}
