import { Button, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userLocalStorage } from "../../services/localStorageService";
import { userService } from "../../services/userService";
// import { SET_USER_INFOR } from "../redux/constant/userConstant";
import pepeAnimation from "../../assets/pepe.json";
import Lottie from "lottie-react";
import {
  setLoginAction,
  setLoginActionService,
} from "../redux/actions/userActions";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        dispatch(setLoginAction(res.data.content));
        message.success("Login success!");

        // Lưu vào localStorage
        userLocalStorage.set(res.data.content);
        // userLocalStorage là object bên file localStorage

        setTimeout(() => {
          navigate("/");
        }, 1200);
      })
      .catch((err) => {
        console.log(err);
        message.error("You dumbass!");
      });
  };

  const onFinishReduxThunk = (value) => {
    let onSuccess = () => {
      setTimeout(() => {
        navigate("/");
      }, 1200);
    };
    dispatch(setLoginActionService(value, onSuccess));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="w-screen h-screen flex justify-center items-center">
      <div className="container p-5 flex">
        <div className="h-full w-1/2">
          <Lottie animationData={pepeAnimation} loop={true} />
        </div>
        <div className="h-full w-1/2">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                span: 24,
              }}
            >
              <Button
                className="bg-red-500 text-white hover:bg-white hover:text-red-500"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
