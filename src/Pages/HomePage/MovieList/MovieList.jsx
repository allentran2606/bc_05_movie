import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";

export default function MovieList({ movieArr }) {
  //   const { Meta } = Card;
  const renderMovieList = () => {
    return movieArr.slice(0, 15).map((item, index) => {
      let { hinhAnh, maPhim } = item;
      return (
        <Card
          key={index}
          hoverable
          style={{
            width: 240,
          }}
          cover={
            <img alt="example" src={hinhAnh} className="h-80 object-cover" />
          }
        >
          {/* <Meta
            title={tenPhim}
            // description={moTa.length < 70 ? moTa : moTa.slice(0, 70) + "..."}
          /> */}
          <NavLink
            to={`/detail/${maPhim}`}
            className="bg-red-500 px-5 py-2 rounded text-white shadow hover:bg-white hover:text-red-500 transition"
          >
            Xem chi tiết
          </NavLink>
        </Card>
      );
    });
  };

  return <div className="grid grid-cols-5 gap-5">{renderMovieList()}</div>;
}
