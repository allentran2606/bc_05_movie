import React, { useEffect, useState } from "react";
import { movieService } from "../../../services/movieService";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";

const onChange = (key) => {
  console.log(key);
};

export default function MovieTabs() {
  let [dataMovie, setDataMovie] = useState([]);

  useEffect(() => {
    movieService
      .getHeThongRap()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    return () => {};
  }, []);

  let renderCumRap = (lstCumRap) => {
    return lstCumRap.map((cumRap) => {
      return {
        label: (
          <div>
            {" "}
            <p>{cumRap.tenCumRap}</p>{" "}
          </div>
        ),
        key: cumRap.maCumRap,
        children: cumRap.danhSachPhim.map((phim) => {
          return <MovieTabItem movie={phim} />;
        }),
      };
    });
  };

  let renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: <img className="w-16 h-16" src={heThongRap.logo} alt="true" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            tabPosition="left"
            defaultActiveKey="1"
            // onChange={onChange}
            items={renderCumRap(heThongRap.lstCumRap)}
          />
        ),
      };
    });
  };

  return (
    <div>
      <Tabs
        tabPosition="left"
        defaultActiveKey="1"
        // onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
