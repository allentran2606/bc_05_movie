import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalStorage } from "../../services/localStorageService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  let handleLogout = () => {
    userLocalStorage.remove();
    window.location.href = "/login";
  };

  const renderUserNav = () => {
    if (user) {
      // Kiểm tra user đã đăng nhập hay chưa
      return (
        <>
          <span>{user.hoTen}</span>
          <button
            className="border-2 border-black rounded px-5"
            onClick={handleLogout}
          >
            Logout
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to={"/login"}>
            <button className="border-2 border-black rounded px-5">
              Login
            </button>
          </NavLink>
          <button className="border-2 border-black rounded px-5">
            Sign up
          </button>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderUserNav()}</div>;
}
